def buildEcho() {
    echo "building jar"
    sh 'mvn --version'
}

def buildImageEcho() {
    echo "building image"
}

def runTests() {
    echo "Running tests.."
}

def buildJar() {
    echo "building the application..."
    sh 'mvn clean package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t ces0712/java-repo:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push ces0712/java-repo:jma-2.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this